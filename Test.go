package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func SlpitStringArr(InputText string) []string {
	SplitString := strings.Split(InputText, " ")
	StringArr := []string{}
	//if len(SplitString) == 3 {
	for _, elem := range SplitString {
		if elem != " " {
			StringArr = append(StringArr, elem)
		}
	}

	//} else {
	//	fmt.Print(("В калькуляторе можно использовать только 2 оперенда"))
	//}

	return StringArr
}

var RomanNumerals = map[string]int{
	"I":  1,
	"IV": 4,
	"V":  5,
	"IX": 9,
	"XL": 40,
	"X":  10,
	"L":  50,
	"C":  100,
	"CD": 400,
	"D":  500,
	"M":  1000,
}

var NumeralsRoman = map[int]string{
	1:    "I",
	4:    "IV",
	5:    "V",
	9:    "IX",
	10:   "X",
	40:   "XL",
	50:   "L",
	100:  "C",
	400:  "CD",
	500:  "D",
	1000: "M",
}

var IteratingValuesInt = []int{
	1000,
	500,
	400,
	100,
	50,
	40,
	10,
	9,
	5,
	4,
	1,
}

func romanToInt(s string) int {
	sum := 0
	greatest := 0
	for i := len(s) - 1; i >= 0; i-- {
		letter := s[i]
		num := RomanNumerals[string(letter)]
		if num >= greatest {
			greatest = num
			sum = sum + num
			continue
		}
		sum = sum - num
	}
	return sum
}

func IntToRoman(result int) string {
	var roman string
	for result > 0 {
		n := FindingMaxInt(result)
		roman += NumeralsRoman[n]
		result -= n
	}
	return roman

}

func FindingMaxInt(result int) int {
	for _, v := range IteratingValuesInt {
		if v <= result {
			return v
		}
	}
	return 1
}

func arithmetiс(StringArr []string) int {

	var result, num1, num2 int

	num1, _ = strconv.Atoi(StringArr[0])
	num2, _ = strconv.Atoi(StringArr[2])
	if (num1 < 1 || num1 > 10) || (num2 < 1 || num2 > 10) {
		fmt.Print("Доступен ввод арабских цифр только от 1 до 10")
	} else {
		switch StringArr[1] {
		case "+":
			fmt.Print(num1 + num2)
		case "-":
			fmt.Print(num1 - num2)
		case "*":
			fmt.Print(num1 * num2)
		case "/":
			if num2 == 0 {
				fmt.Print("На ноль делить нельзя")

			} else {
				fmt.Print(num1 / num2)
			}
		}
	}

	return result
}

func arithmetiсRim(StringArr []string) string {

	var result, num1, num2 int

	num1 = romanToInt(StringArr[0])
	num2 = romanToInt(StringArr[2])
	if num1 < 1 || num1 > 10 || num2 < 1 || num2 > 10 {
		fmt.Print("Доступен ввод римских цифр только от 1 до 10")
	} else if StringArr[0] != IntToRoman(num1) || StringArr[2] != IntToRoman(num2) {
		fmt.Print("Неверно введено рииское число")
	} else {
		switch StringArr[1] {
		case "+":
			result = num1 + num2
		case "-":
			result = num1 - num2

		case "*":
			result = num1 * num2
		case "/":
			result = num1 / num2
		}

	}
	if result < 0 {
		fmt.Print("Римские числа не могут иметь значение меньше 1")
	}
	return IntToRoman(result)
}

func main() {
	//var equation string = fmt.Scan(&equation)
	//fmt.Println(equation)
	//fmt.Fscan(os.Stdin, &equation)

	equation1 := SlpitStringArr(Scan1())
	//equation := "II+II"
	//equation1 := SlpitStringArr(equation)
	if len(equation1) == 3 {
		_, err1 := strconv.Atoi(equation1[0])
		_, err2 := strconv.Atoi(equation1[2])

		if err1 == nil && err2 == nil {
			arithmetiс(equation1)
		} else if (err1 == nil && err2 != nil) || (err2 == nil && err1 != nil) {
			fmt.Print("Неверные данные при вводе")
		} else {
			fmt.Print(arithmetiсRim(equation1))
		}
	} else if len(equation1) < 3 {
		fmt.Print("Неверный формат ввода")
	} else {
		fmt.Print("В калькуляторе нельзя использовать больше 3ех оперендов")
	}

}

func Scan1() string {
	in := bufio.NewScanner(os.Stdin)
	in.Scan()
	if err := in.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Ошибка ввода:", err)
	}
	return in.Text()
}
